const fs = require('fs');

let autores = [
	'Director A', 'Director B', 'Director C', 'Director D', 'Director E', 
	'Director F', 'Director G', 'Director H', 'Director I', 'Director J'
];

let titulos = [
'La venganza de goku', 'Amor y jamburgesa', 'Jhon walter', 'El esfero asesino', 'gg ez nub'
];

let estados = [1, 2]

let libros = [];

let libro;

for (let i = 1; i <= 500; i++) {
	libro = {};
	
	libro.id = i;
	libro.autor = autores[Math.floor(Math.random() * autores.length)];
	libro.titulo = titulos[Math.floor(Math.random() * titulos.length)] + ' ' + i;
	libro.estado = estados[Math.floor(Math.random() * estados.length)];

	let referencia = i;
	libro.referencia = 'PEL-' + referencia.toString();

	libros.push(libro);
}

if(!fs.existsSync('peliculas.json')){
	fs.writeFile('peliculas.json', JSON.stringify(libros), (err) => {
			if (err) {
				return console.error(err);
		 }
		 console.log("Data written for pelis successfully!");
	});
}

if(!fs.existsSync('rentas.json')){
	fs.writeFile('rentas.json', '[{}]', (err) => {
			if (err) {
				return console.error(err);
		 }
		 console.log("Data written for rentas successfully!");
	});
}
