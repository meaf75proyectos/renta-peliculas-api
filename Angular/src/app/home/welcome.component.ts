import { Component } from '@angular/core';

import { ProductService } from "../usuarios/usuario.service";

@Component({
    templateUrl: './welcome.component.html'
})
export class WelcomeComponent {
    
    rentas: any[];

    constructor(private productService: ProductService) {
        this.productService.rentasListar().subscribe((response: any) => {
            this.rentas = response;
        });
    }
}
